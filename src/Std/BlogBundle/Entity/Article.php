<?php

namespace Std\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Article
 *
 * @ORM\Table(name="Std_article")
 * @ORM\Entity(repositoryClass="Std\BlogBundle\Entity\ArticleRepository")
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;
    
    
    
    /**
     * @var string $titre
     *
     * @ORM\Column(name="titre", type="string", length=80)
     * @Assert\Length(
     *      min = "5",
     *      max = "50",
     *      minMessage = "Votre titre doit faire au moins {{ limit }} caract�res",
     *      maxMessage = "Votre titre ne peut pas �tre plus long que {{ limit }} caract�res"
     * )
     */
    private $titre;

    /**
     * @var text $contenu
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", length=80, nullable=true)
     * @Assert\NotBlank(message="pas blanc")
     */
    private $auteur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datecreation", type="datetime", nullable=true)
     */
    private $datecreation;


    /**
     * @var \boolean
     *
     * @ORM\Column(name="publication", type="boolean")
     */
    private $publication;
    
    
   
    /**
     *
	 * @ORM\OneToMany(targetEntity="Std\BlogBundle\Entity\Article2", mappedBy="Std\BlogBundle\Entity\Article")
     */
    private $article2s;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Article
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     * @return Article
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set datecreation
     *
     * @param \DateTime $datecreation
     * @return Article
     */
    public function setDatecreation($datecreation)
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    /**
     * Get datecreation
     *
     * @return \DateTime 
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }
    

        
    
    public function __construct(){
    	$this->datecreation = new \DateTime();
    	$this->setPublication(true);
    }
    

    /**
     * Set publication
     *
     * @param boolean $publication
     * @return Article
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return boolean 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add article2s
     *
     * @param \Std\BlogBundle\Entity\Article2 $article2s
     * @return Article
     */
    public function addArticle2(\Std\BlogBundle\Entity\Article2 $article2s)
    {
        $this->article2s[] = $article2s;

        return $this;
    }

    /**
     * Remove article2s
     *
     * @param \Std\BlogBundle\Entity\Article2 $article2s
     */
    public function removeArticle2(\Std\BlogBundle\Entity\Article2 $article2s)
    {
        $this->article2s->removeElement($article2s);
    }

    /**
     * Get article2s
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArticle2s()
    {
        return $this->article2s;
    }
}
