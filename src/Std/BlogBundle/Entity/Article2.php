<?php

namespace Std\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article2
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Std\BlogBundle\Entity\Article2Repository")
 */
class Article2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="paragraphe", type="text")
     */
    private $paragraphe;


    /**
     *
 	 * @ORM\ManyToOne(targetEntity="Std\BlogBundle\Entity\Article", inversedBy="Std\BlogBundle\Entity\Article2")
     */
    private $article;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paragraphe
     *
     * @param string $paragraphe
     * @return Article2
     */
    public function setParagraphe($paragraphe)
    {
        $this->paragraphe = $paragraphe;

        return $this;
    }

    /**
     * Get paragraphe
     *
     * @return string 
     */
    public function getParagraphe()
    {
        return $this->paragraphe;
    }
}
