<?php
// src/Std/BlogBundle/DataFixtures/ORM/LoadArticleData.php
namespace Std\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Std\BlogBundle\Entity\Article;


//php app/console doctrine:fixtures:load
class LoadArticleData implements FixtureInterface {
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager) {
		
		//---remplissage de 10 lignes dans articles
		for($i = 0; $i < 5; $i ++) {
			$articleAdmin = new Article ();
			$articleAdmin->setTitre ( "Titre $i" );
			$articleAdmin->setContenu ( 'blablabla' );
			
			$manager->persist ( $articleAdmin );
			$manager->flush ();
		}
	}
}