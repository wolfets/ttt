<?php

namespace Std\BlogBundle\Services;

use Doctrine\ORM\Event\LifecycleEventArgs;

class Slugger {

	private $slugger;
	
	public function setSlugger($slugger) {
		$this->slugger = $slugger;
		return $this;
	}
	
	public function preUpdate(LifecycleEventArgs $args) {
		return $this->prePersist ( $args );
	}
	
	public function prePersist(LifecycleEventArgs $args) {
		$entity = $args->getEntity ();
		
		// peut-�tre voulez-vous seulement agir sur une entit� � Article �
		if ($entity instanceof Article) {
			$slug = $this->slugger->getSlug ( $entity->getTitre () );
			$entity->setSlug ( $slug );
		}
	}
}