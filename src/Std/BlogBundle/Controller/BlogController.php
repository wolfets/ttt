<?php

namespace Std\BlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Std\BlogBundle\Entity;
use Std\BlogBundle\Entity\Article;
use Doctrine\ORM\EntityManager;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 *
 * @Route("/blog", name="blog")

 */
class BlogController extends Controller
{
	/**
	 * Lists all Article entities.
	 *
	 * @Template("StdBlogBundle:Blog:index.html.twig")
	 * @ParamConverter()
	 */
    public function indexAction($page)
    {
    	// On r�cup�re le repository
    	$em= $this->getDoctrine()
    	->getManager()
    	->getRepository('StdBlogBundle:Article');
    	 
    	// On r�cup�re l'entit� correspondant � l'id $id
    	$articles = $em->findAll();
    	
       // return $this->render('StdBlogBundle:Blog:index.html.twig', Array('articles'=>$articles));
    	return Array('articles'=>$articles, "mot"=>"le mot");
    }
    
    
    /**
     * Lists all Article entities.
     *@Route("/article/{id}", name="std_blog_voir", requirements={"id" = "\d+"})
     *@Route("/article/{slug}", name="std_blog_voir_slug")
     *
     * @Template("StdBlogBundle:Blog:voir.html.twig")
     * @ParamConverter()
     */
    public function voirAction(Article $article)
    {
    	//return $this->render('StdBlogBundle:Blog:voir.html.twig',array('id'=>$id));
    	return array('article'=>$article, "mot"=>"le mot");
    }
    
   
    public function ajouterAction()
    {
    	$article = new Article();
    	$article->setTitre("Hello World")->setAuteur("Toto")->setContenu("");
    	
    	$article2 = new Article();
    	$article2->setTitre("Hello World2222")->setAuteur("Toto2222")->setContenu("");
    	
    	$em = $this->getDoctrine()->getManager();
    	$em->persist($article);//$em   d�marre une transaction
    	$em->persist($article2);
    	
    	$em->flush();
    	
    	
    	return $this->render('StdBlogBundle:Blog:ajouter.html.twig', Array('article'=>$article));
    }
 
    public function modifierAction($id)
    {
    	// On r�cup�re le repository
    	$em= $this->getDoctrine()
    	->getManager()
    	->getRepository('StdBlogBundle:Article');
    	
    	// On r�cup�re l'entit� correspondant � l'id $id
    	$article = $em->find($id);

    	
    	// Ou null si aucun article n'a �t� trouv� avec l'id $id
    	if($article === null)
    	{
    		throw $this->createNotFoundException('Article[id='.$id.']inexistant.');
    	}
    	

    	return $this->render('StdBlogBundle:Blog:modifier.html.twig', array('article'=>$article));
    }
  
    public function deleteAction($id)
    {
    	// On r�cup�re le repository
    	$em= $this->getDoctrine()
    	->getManager();
    	//->getRepository('StdBlogBundle:Article');
    	 
    	// On r�cup�re l'entit� correspondant � l'id $id
    	$article = $em->find('StdBlogBundle:Article', $id);

    	 
    	// Ou null si aucun article n'a �t� trouv� avec l'id $id
    	if($article === null)
    	{
    		throw $this->createNotFoundException('Article[id='.$id.']inexistant.');
    	}
    	
    	$em->remove($article);
    	$em->flush();
    	
    	return $this->render('StdBlogBundle:Blog:delete.html.twig',array('article'=>$article));
    }
    

    public function menugaucheAction()
    {
//     	$articles = array(
//     			array('titre'=>"article 1", 'contenu'=>"blablabla"),    			
//     			array('titre'=>"article 2", 'contenu'=>"nanani"),
//     			array('titre'=>"article 3", 'contenu'=>"lalala")
    			
//     	);

    	
    	// On r�cup�re le repository
    	$em= $this->getDoctrine()
    	->getManager()
    	->getRepository('StdBlogBundle:Article');
    	
    	// On r�cup�re l'entit� correspondant � l'id $id
    	$articles = $em->findAll();
    	return $this->render('StdBlogBundle:Blog:menugauche.html.twig', array('articles'=>$articles));
    }
}
