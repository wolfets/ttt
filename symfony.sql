-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 16 Juin 2015 à 11:44
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `symfony`
--

-- --------------------------------------------------------

--
-- Structure de la table `article2`
--

CREATE TABLE IF NOT EXISTS `article2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paragraphe` longtext COLLATE utf8_unicode_ci NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BE06406E7294869C` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `std_article`
--

CREATE TABLE IF NOT EXISTS `std_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci NOT NULL,
  `auteur` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datecreation` datetime DEFAULT NULL,
  `publication` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Contenu de la table `std_article`
--

INSERT INTO `std_article` (`id`, `titre`, `contenu`, `auteur`, `datecreation`, `publication`) VALUES
(23, 'Titre 0', 'blablabla', NULL, '2015-06-16 10:36:51', 1),
(24, 'Titre 1', 'blablabla', NULL, '2015-06-16 10:36:51', 1),
(25, 'Titre 2', 'blablabla', NULL, '2015-06-16 10:36:51', 1),
(26, 'Titre 3', 'blablabla', NULL, '2015-06-16 10:36:51', 1),
(27, 'Titre 4', 'blablabla', NULL, '2015-06-16 10:36:51', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article2`
--
ALTER TABLE `article2`
  ADD CONSTRAINT `FK_BE06406E7294869C` FOREIGN KEY (`article_id`) REFERENCES `std_article` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
